package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.endpoint.AccountServiceEndpoint;
import com.example.protocol.Account;

@Component
public class Client {
  @Autowired
  private AccountServiceEndpoint accountServiceEndpoint;

  public void insertAccounts() {

    for(int i = 0; i < 10; ++i) {
      Account account = new Account();
      account.setName(String.format("Имя счета %d", i));
      accountServiceEndpoint.insertAccount(account);
    }
  }
}
