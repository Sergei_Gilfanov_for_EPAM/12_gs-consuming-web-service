package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.endpoint.AccountService;
import com.example.endpoint.AccountServiceEndpoint;

@SpringBootApplication
public class Main {

  @Bean
  AccountService accountService() {
    return new AccountService();
  }

  @Bean
  AccountServiceEndpoint accountServiceEndpoint(AccountService accountService) {
    return accountService.getAccountServiceEndpointPort();
  }

  @Bean(initMethod="insertAccounts")
  Client client() {
    return new Client();
  }

  public static void main(String[] args) {
   SpringApplication.run(Main.class, args);
  }

}
